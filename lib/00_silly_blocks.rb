def reverser
	yield.split.map(&:reverse).join(" ")
end

def adder(increment=1)
	val = yield
	val += increment
end

def repeater(reps = 1)
	reps.times do
		yield
	end
end

