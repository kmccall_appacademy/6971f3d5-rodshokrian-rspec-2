def measure(num_of_runs=1)
	start_time = Time.now
	num_of_runs.times {yield}
	(Time.now - start_time)/num_of_runs
end

